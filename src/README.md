---
title: Home
home: true
heroImage: /img/speaker02.png
heroText: Hi, I'm Brendan 😃
tagline: Experienced Servant Leader & DevOps Expert 
actionText: Get in touch
actionLink: /contact/
features:
- 
    title: Speaker
    href: /talks/
    details: Not your typical tech speaker, my talks include interesting takes on technology's influence and power in society today.  From the Apollo program to Legos to Black Mirror, you won't find a "normal boring slide deck with code" here.
- 
    title: Technologist
    href: /portfolio/
    details: Knowing "just enough to be dangerous" has always been my passion. From bricking my first Tandy computer (sorry Dad) to building side apps people love, I have professional and personal experience tinkering and building from scratch. 
- 
    title: DevOps Expert
    href: "/work-philosophy/"
    details: Best practices are truly best when they based on real-world experience.  My philosophy on software delivery comes from hard-won years of experience in healthcare, defense, and enterprises large and small.  I've seen what works and - more importantly - what doesn't.
#footer: This is a footer [foo](bar.com)
---


# Work
My entire career, my passion has been driven by the ability to level up engineers. 

Originally this was as an Engineering and Product Manager at several software companies, 
but more recently, I've been privileged to work on developer tooling directly at GitLab. 
In 2020, I changed roles to be a [Developer Evangelist](https://about.gitlab.com/job-families/marketing/developer-evangelist/) at GitLab, and you can find my [2020 Developer Evangelist Plan here](https://gitlab.com/brendan/2020-dev-evangelism).

Learn more about my experience by viewing [my resume](/resume/) or [get in touch](/contact/).

## Talks
Please take a look at some of the [talks I've given](/talks/). There may also be some that are still a work in progress 😉.

## Portfolio 
My [portfolio](/portfolio/) contains projects both past and present - and all have helped shaped my experience.

## Philosophy
See [my work philosophy](/work-philosophy/).

# Books I ❤️
See [books](/books/) for a list of books I love.

# Portfolio
See [my portfolio](/portfolio/) for more details on past and present work.
