---
title: Talks
---

# Upcoming & Previous Talks
See my [events](/events/) page for upcoming events.  I even have what events I've submitted talks to buy may or may not be accepted to in the spirt of [transparency](https://about.gitlab.com/handbook/values/#transparency).  You can also check out my [speaker bio](/talks/bio).

# Featured Videos

<FeaturedVideos
    :videos="[
        { title: 'The Asynchronous Enterprise', youtubeId: 'xRro-dDsYPU' },
        { title: 'Failure is not an option', youtubeId: 'cRGjw04ZA4M' },
    ]"
/>

# Abstracts & Previous Events

<TalkList
    :pages="$site.pages" 
/>

