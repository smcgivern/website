---
title: "I'm not technical enough to give this talk"
talk: true
tagline: "I don't have a computer science degree.  That doesn't mean I can't teach what I know...and in some ways, it means I have a unique ability to do just that, and you do too."
abstract: |
    I don't have a computer science degree.  I haven't ever been paid to write code for a living. In fact, the only programming "class" I've ever taken was a VB business school class 15 years ago.

    In this talk, I'll prove that the qualifications to learn, teach, and talk about technology are not the "traditional" ones.  No one should feel that their opinion or experience is less than someone who has more of a typical software engineering resume.  By giving three mini-demos of technology that I have no right talking about, I'll show that being self-taught doesn't mean you shouldn't share your knowledge with others.  The only requirement is a little creativity and a lot of curiosity, and anyone can stand up on stage or write a blog or teach their coworkers something new.
#slides: https://docs.google.com/presentation/
#video: https://www.youtube.com/
#events:
#  - name: A Great Event
#    date: "2020-02-02"
#    href: https://www.youtube.com/
---

<Talk :talk="$page.frontmatter"/>
