---
title: "Three keys to making the right multicloud decisions"
talk: true
tagline: The question isn't about if you will be a multi- or hybrid-cloud company. The question is - are you ready to be better at it than your competition.
abstract: |
    Business survival depends on adaptive and efficient software development practices. Those practices require several things that necessitate a multi-cloud strategy. Workload portability is essential. Ensuring the ability to negotiate with suppliers is critical. The ability to use the best tools for the job is equally as important. The cloud promises to remove undifferentiated work from our teams. To realize that potential, we must have a measured approach.

    The question isn't about if you will be a multi- or hybrid-cloud company. The question is: are you ready to be better at it than your competition. In this talk, we'll discuss the three keys to enabling your business to make the right cloud decisions: visibility, efficiency, and governance. These capabilities will help you operate with confidence across the multi-cloud and hybrid-cloud landscape.
slides: https://docs.google.com/presentation/d/e/2PACX-1vSZCc-h4WVyjAsAMVv2Ufx_0mNn-HRSRETh-Y2Pq6N4dPCFs3nSi5VuT_APtmuREN0Htwxyali9ribr/embed
video: https://www.youtube.com/embed/Qwins6ZScCo
events:
  - name: ESCAPE/19 the multi-cloud conf
    date: "2019-10-16"
    href: https://www.youtube.com/watch?v=cRGjw04ZA4M
---

<Talk :talk="$page.frontmatter"/>
