---
title: "Brendan O'Leary Speaker Bio"
---

# Brendan's Speaker Bio & Media Resources

> NOTE: this page is mostly for me.
>
> It is so that I remember where I have all of these things when I need it.  No one else really cares about having all of my headshots...I don't think 😃

## One Sentance Bio

Brendan O'Leary is a Senior Developer Evangelist for GitLab who connects with developers, contributes to open source projects, and shares his work with about cutting-edge technologies on conference panels, meetups, in contributed articles and on blogs.

## Standard Bio

Brendan O'Leary is a Senior Developer Evangelist at GitLab, the first single application for the DevSecOps lifecycle. He has a passion for software development and iterating on processes just as quickly as we iterate on code. Working with customers to deliver value is what drives Brendan's passion for DevOps and smooth CI/CD implementation. Brendan has worked with a wide range of customers - from the nation's top healthcare institutions to environmental services companies to the Department of Defense. Outside of work, you'll find Brendan with 1 to 4 kids hanging off of him at any given time or occasionally finding a moment alone to build something in his workshop.

## Media

### Profile Images

<table>
    <tr>
        <td>
            <BlogImage image="/img/speaker02.png" caption="Speaking at GitLab Contribute in New Orleans" />
        </td>
        <td>
            <BlogImage image="/img/brendan2020.jpg" caption="2020 Headshot" />
        </td>
    </tr>
    <tr>
        <td>
            <BlogImage image="/img/brendan_avatar.png" caption="Avatar with transparent backgrond" />
        </td>
        <td>
            <BlogImage image="/img/brendan_avatar_blue.png" caption="Avatar with blue backgrond" />
        </td>
    </tr>
    <tr>
        <td>
            <BlogImage image="/img/headshot.png" caption="Older headshot...more hair" />
        </td>
        <td>
            <BlogImage image="/img/headshot2.png" caption="Speaking at DevOps Days Baltimore" />
        </td>
    </tr>
</table>

### Streaming Media


<table>
    <tr>
        <td>
            <BlogImage image="/img/twitch-frame.png" caption="Twitch streaming frame" />
        </td>
        <td>
            <BlogImage image="/img/tanuki-color-outline.png" caption="Color outline tanuki" />
        </td>
    </tr>
</table>