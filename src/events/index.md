---
title: Events
events:
# Required fields: name, start_date, attending (yes, no, maybe), speaking (yes, no, submitted)
# Optional fiels: location, end_date

  - name:       GitLab Connect Philadelphia
    start_date: "2020-03-11"
    location:   Philadelphia, PA
    attending:  'yes'
    speaking:   'yes'

  - name:       GDIT Emerge
    start_date: "2020-04-14"
    location:   Washington, D.C.
    attending:  'yes'
    speaking:   'yes'

  - name:       Agile DevOps West
    start_date: "2020-06-09"
    end_date:   "2020-06-11"
    location:   Virtual!
    attending:  'yes'
    speaking:   'yes'
    #href:

  - name:       DevOps Enterprise Summit 2020
    start_date: "2020-11-09"
    end_date:   "2020-11-11"
    location:   Las Vegas, Nevada
    attending:  'yes'
    speaking:   'submitted'

  - name:       Grace Hopper Celebration
    start_date: "2020-10-29"
    end_date:   "2020-11-02"
    location:   Virtual
    attending:  'maybe'
    speaking:   'no'
    #href:

  - name:       KubeCon North America
    start_date: "2020-11-17"
    end_date:   "2020-11-20"
    location:   Boston, MA
    attending:  'yes'
    speaking:   'maybe'
    #href:

## Past

  - name:       The Linux Foundation Member Summit
    start_date: "2020-03-10"
    end_date:   "2020-03-12"
    location:   Lake Tahoe, California
    attending:  'no'
    speaking:   'submitted'
    href:       https://events.linuxfoundation.org/lf-member-summit/

  - name:       KubeCon EU 2020
    start_date: "2020-03-30"
    end_date:   "2020-04-02"
    location:   Amsterdam, The Netherlands
    attending:  'yes'
    speaking:   'submitted'
    #href:       

  - name:       JSConf Hawaii
    start_date: "2020-01-04"
    end_date:   "2020-01-07"
    location:   Honolulu, Hawaii
    attending:  'yes'
    speaking:   'yes'

  - namw:       AWS re:Invent
    start_date: "2019-12-02"
    end_date:   "2019-12-06"
    location:   Las Vegas, Nevada
    attending:  'yes'
    speaking:   'no'

  - name:       ESCAPE/19 the multi-cloud conf
    start_date: "2019-10-16"
    end_date:   "2019-10-17"
    location:   New York, New York
    attending:  'yes'
    speaking:   'yes'
    #href:
  
  - name:       GitLab Commit
    start_date: "2019-09-17"
    location:   Brookyln, New york
    attending:  'yes'
    speaking:   'no'

  - name:       VMWorld
    start_date: "2019-08-26"
    end_date:   "2019-08-27"
    location:   San Francisco, California
    attending:  'yes'
    speaking:   'no'

  - name:       Refactr.tech
    start_date: "2019-06-06"
    end_date:   "2019-06-07"
    location:   Atlanta, Georgia
    attending:  'yes'
    speaking:   'no'

  - name:       DevOps Days Baltimore 2019
    start_date: "2019-04-24"
    end_date:   "2019-04-25"
    location:   Baltimore, Maryland
    attending:  'yes'
    speaking:   'yes'
  
  - name:       Google Next
    start_date: "2019-04-08"
    end_date:   "2019-04-12"
    location:   San Francisco, California
    attending:  'yes'
    speaking:   'no'

  - name:       Microsoft Ignite DC
    start_date: "2019-02-04"
    end_date:   "2019-02-05"
    location:   Washington, D.C.
    attending:  'yes'
    speaking:   'no'

  - name:       DevOps Days Baltimore 2018
    start_date: "2018-03-22"
    end_date:   "2018-03-23"
    location:   Baltimore, Maryland
    attending:  'yes'
    speaking:   'yes'



---

# Events
Find events I'm attending that are coming up. I even have what events I've submitted talks to buy may or may not be accepted to in the spirt of [transparency](https://about.gitlab.com/handbook/values/#transparency).  For abstracts and videos, see [talks](/talks/).

## Upcoming Events
<EventsList :events="$frontmatter.events" :future="true"/>

## Past Events
<EventsList :events="$frontmatter.events" :future="false"/>
