---
title: Books & Media
---

# Books I ❤️

* [Joel on Software](https://www.amazon.com/Joel-Software-Occasionally-Developers-Designers/dp/1590593898/ref=sr_1_1?keywords=joel+on+software&qid=1565900156&s=gateway&sr=8-1)
* [Accelerate: The Science of Lean Software and DevOps](https://www.amazon.com/Accelerate-Software-Performing-Technology-Organizations/dp/1942788339/ref=sr_1_1?keywords=accelerate+book&qid=1565900507&s=gateway&sr=8-1) by Nicole Forsgren, Ph.D., Jez Humble, and Gene Kim
* The [GitLab Product Handbook](https://about.gitlab.com/handbook/product/)
* [The Developers Guide to Content Creation](https://www.developersguidetocontent.com/) by [Stephanie Morillo](https://twitter.com/radiomorillo)
* [The Hard Thing About Hard Things](https://www.amazon.com/Hard-Thing-About-Things-Building/dp/0062273205/ref=sr_1_2?crid=3OWTEPOESX1IQ&keywords=hard+thing+about+hard+things+book&qid=1565900207&s=gateway&sprefix=hard+thing+about%2Caps%2C124&sr=8-2) by Ben Horowitz
* [How to Win Friends & Influence People](https://www.amazon.com/How-Win-Friends-Influence-People/dp/0671027034/ref=sr_1_3?crid=1Q37IHR90QNN2&keywords=how+to+win+friends+and+influence+people+by+dale+carnegie&qid=1565900213&s=gateway&sprefix=how+to+win%2Caps%2C124&sr=8-3)
* Malcolm Gladwell's [The Tipping Point](https://www.amazon.com/Tipping-Point-Little-Things-Difference/dp/0316346624/ref=sr_1_5?crid=20G6E8QVQI0NY&keywords=malcom+gladwell&qid=1565900180&s=gateway&sprefix=malcom%2Caps%2C125&sr=8-5)
* [Extreme Ownership](https://www.amazon.com/Extreme-Ownership-U-S-Navy-SEALs/dp/B015TM0RM4/ref=sr_1_3?crid=1JM4W67ZDUTL4&keywords=extreme+ownership&qid=1565900683&s=gateway&sprefix=extreme+own%2Caps%2C126&sr=8-3): How U.S. Navy SEALs Lead and Win
