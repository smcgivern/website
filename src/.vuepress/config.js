const currentDateUTC = new Date().toUTCString()

module.exports = {
	title: 'Brendan O\'Leary',
	dest: './public',
	
	themeConfig: {
		repoLabel: 'Repo',
		editLinks: true,
		editLinkText: 'Found a bug? Help me improve this page!',
		nav: [
			{ text: 'Home', link: '/' }, 
			{ text: 'Talks', link: '/talks/'},
			{ text: 'Portfolio', link: '/portfolio/' },
			{ text: 'Blog', link: '/blog/' },
			{
				text: 'About me', 
				items: [
					{ text: 'My Work Philosophy', link: '/work-philosophy/' },
					{ text: 'My Resume', link: '/resume/' },
					{ text: 'Side Projects', link: '/side-projects/' },
					{ text: 'Events', link: '/events/' },
					{ text: 'Books & Media', link: '/books/' },
					{ text: 'Blog Archive', link: '/archive/' },
				]
			},
			{ text: 'Contact', link: '/contact/' },
		],
		logo: '/brendan_avatar.png',
		docsDir: 'src',
		pageSize: 5,
		startPage: 0,
		search: false,
		smoothScroll: true,
	},
	plugins: [
		[
			'@vuepress/google-analytics',
			{
				ga: 'UA-4180169-5' // UA-00000000-0
			}
		],
		[
			'vuepress-plugin-rss',
			{
				base_url: '/',
				site_url: 'https://boleary.dev',
				filter: frontmatter => frontmatter.date <= new Date(currentDateUTC),
				count: 20
			}
		],
		'vuepress-plugin-reading-time',
		'vuepress-plugin-janitor'
	],
	head: [
		['link', { rel: 'apple-touch-icon', sizes: '180x180', href: '/apple-icon.png' }],
		['link', { rel: 'icon', sizes: '32x32', href: '/favicon-32x32.png' }],
		['link', { rel: 'icon', sizes: '16x16', href: '/favicon-16x16.png' }],
		['link', { rel: 'manifest', href: '/site.webmanifest' }],
		['link', { rel: 'mask-icon', href: '/safari-pinned-tab.svg', color: '#5bbad5' }],
		['meta', { name: 'msapplication-TileColor', content: '#da532c' }],
		['meta', { name: 'theme-color', content: '#ffffff' }],
		['script', { src: '/tweet.js' }]
	]
}
